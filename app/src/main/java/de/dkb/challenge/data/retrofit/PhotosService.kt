package de.dkb.challenge.data.retrofit

import de.dkb.challenge.data.model.PhotoModel
import retrofit2.http.GET

interface PhotosService {
    @GET("photos")
    suspend fun listPhotos(): List<PhotoModel>

}
