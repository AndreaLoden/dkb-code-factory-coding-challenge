package de.dkb.challenge.data.retrofit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object PhotosClient {

    private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

    val PHOTOS_SERVICE: PhotosService by lazy {

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return@lazy retrofit.create(PhotosService::class.java)
    }
}