package de.dkb.challenge.data.model

import java.io.Serializable

data class PhotoModel(
    val id: String?,
    val albumId: String?,
    val title: String?,
    val url: String?,
    val thumbnailUrl: String?
) : Serializable
