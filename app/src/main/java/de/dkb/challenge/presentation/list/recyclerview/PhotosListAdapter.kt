package de.dkb.challenge.presentation.list.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.dkb.challenge.R
import de.dkb.challenge.data.model.PhotoModel
import java.util.*

internal class PhotosListAdapter(private val photoClickListener: PhotoClickListener) :
    RecyclerView.Adapter<PhotosListAdapter.PhotoItemViewHolder>() {

    interface PhotoClickListener {
        fun onPhotoClicked(photo: PhotoModel)
    }

    private val photos: MutableList<PhotoModel>

    init {
        photos = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoItemViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val view = inflater.inflate(R.layout.photo_item, parent, false)

        return PhotoItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhotoItemViewHolder, position: Int) {
        holder.bind(photos[position])

        val photo = photos[position]
        ViewCompat.setTransitionName(holder.photoImage, photo.id)
        holder.itemView.setOnClickListener { photoClickListener.onPhotoClicked(photo) }
    }

    override fun getItemCount(): Int = photos.size

    fun setData(photos: List<PhotoModel>) {
        this.photos.clear()
        this.photos.addAll(photos)
    }

    inner class PhotoItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var photoImage: ImageView = itemView.findViewById(R.id.photo_image)
        var photoTitle: TextView = itemView.findViewById(R.id.photo_title)

        fun bind(photoModel: PhotoModel) {

            photoTitle.text = photoModel.title

            Picasso.get()
                .load(photoModel.thumbnailUrl)
                .placeholder(R.drawable.photo_placeholder_loading)
                .error(R.drawable.photo_placeholder_error)
                .into(photoImage)
        }

    }
}
