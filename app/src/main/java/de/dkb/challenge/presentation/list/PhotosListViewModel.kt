package de.dkb.challenge.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.dkb.challenge.data.model.PhotoModel
import de.dkb.challenge.data.retrofit.PhotosClient
import kotlinx.coroutines.launch

class PhotosListViewModel : ViewModel() {

    private val photosLiveData = MutableLiveData<List<PhotoModel>>()

    internal fun getPhotos(): LiveData<List<PhotoModel>> {
        return photosLiveData
    }

    fun loadPhotos() {
        viewModelScope.launch {
            PhotosClient.PHOTOS_SERVICE.listPhotos().also {
                photosLiveData.setValue(it)
            }
        }
    }
}