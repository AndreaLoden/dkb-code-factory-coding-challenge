package de.dkb.challenge.presentation.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import de.dkb.challenge.R
import de.dkb.challenge.data.model.PhotoModel
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_list.toolbar

class PhotoDetailActivity : AppCompatActivity() {

    /**********************************************************************************************
     * Lifecycle callbacks
     *********************************************************************************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)

        val photoModel = intent.extras?.getSerializable(EXTRA_PHOTO_URL) as PhotoModel

        Picasso.get()
            .load(photoModel.thumbnailUrl)
            .placeholder(R.drawable.photo_placeholder_loading)
            .error(R.drawable.photo_placeholder_error)
            .into(photo_image)

        photo_title.text = photoModel.title
    }

    companion object {
        private const val EXTRA_PHOTO_URL = "EXTRA_PHOTO_URL"

        fun getIntent(context: Context, photoModel: PhotoModel): Intent {

            return Intent(context, PhotoDetailActivity::class.java).apply {
                putExtra(EXTRA_PHOTO_URL, photoModel)
            }
        }
    }
}
