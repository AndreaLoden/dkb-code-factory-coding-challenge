package de.dkb.challenge.presentation.list

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import de.dkb.challenge.R
import de.dkb.challenge.data.model.PhotoModel
import de.dkb.challenge.presentation.detail.PhotoDetailActivity
import de.dkb.challenge.presentation.list.recyclerview.PhotosListAdapter
import de.dkb.challenge.presentation.list.recyclerview.SpacesItemDecoration
import kotlinx.android.synthetic.main.activity_list.*

class PhotosListActivity : AppCompatActivity(), PhotosListAdapter.PhotoClickListener {

    private val photosAdapter by lazy { PhotosListAdapter(this) }

    /**********************************************************************************************
     * Lifecycle callbacks
     *********************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_list)
        setSupportActionBar(toolbar)

        setupRecyclerView()

        ViewModelProviders.of(this).get(PhotosListViewModel::class.java).apply {
            getPhotos().observe(this@PhotosListActivity, Observer<List<PhotoModel>> {
                photosAdapter.setData(it)
                photosAdapter.notifyDataSetChanged()
            })
            loadPhotos()
        }
    }

    /**********************************************************************************************
     * Implementation of [PhotosListAdapter.PhotoClickListener]
     *********************************************************************************************/
    override fun onPhotoClicked(photo: PhotoModel) {
        startActivity(PhotoDetailActivity.getIntent(this, photo))
    }

    /**********************************************************************************************
     * Private methods
     *********************************************************************************************/

    private fun setupRecyclerView() {
        val gridLayoutManager = LinearLayoutManager(this)

        photos_recycler_view.layoutManager = gridLayoutManager
        photos_recycler_view.setHasFixedSize(true)
        photos_recycler_view.adapter = photosAdapter
    }
}